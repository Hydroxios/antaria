package fr.Antaria.antaria;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class ItemBuilder {
	
	private ItemStack is;
	
	public ItemBuilder(Material type) {
		is = new ItemStack(type);
	}
	
	public ItemBuilder setAmount(int amount) {
		is.setAmount(amount);
		return this;
	}
	
	public ItemBuilder setDisplayName(String name) {
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		is.setItemMeta(meta);
		return this;
	}
	
	public ItemBuilder setLore(List<String> lore) {
		ItemMeta meta = is.getItemMeta();
		meta.setLore(lore);
		is.setItemMeta(meta);
		return this;
	}
	
	public ItemBuilder setGlowing(Plugin plugin) {
		ItemMeta meta = is.getItemMeta();
		NamespacedKey key = new NamespacedKey(plugin, plugin.getDescription().getName());
		Glow glow = new Glow(key);
		meta.addEnchant(glow, 1, true);
		is.setItemMeta(meta);
		return this;
	}
	
	public ItemStack build() {
		return this.is;
	}

}
