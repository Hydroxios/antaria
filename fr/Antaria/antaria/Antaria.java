package fr.Antaria.antaria;

import java.lang.reflect.Field;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;

import fr.Antaria.antaria.listeners.PlayerListener;

public class Antaria extends JavaPlugin {

	@Override
	public void onEnable() {
		registerGlow();
		registerCrafts();
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
	}

	@Override
	public void onDisable() {

	}

	@SuppressWarnings("deprecation")
	private void registerCrafts() {
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("�lI believe I can fly !!");
		ItemStack flyChest = new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).setDisplayName("�cGliding Chestplate").setLore(lore).setGlowing(this).build();

		ShapedRecipe flyChestC = new ShapedRecipe(flyChest);

		flyChestC.shape("SEN", "ECE", "NES");

		flyChestC.setIngredient('S', Material.SHULKER_SHELL);
		flyChestC.setIngredient('E', Material.ELYTRA);
		flyChestC.setIngredient('C', Material.LEATHER_CHESTPLATE);
		flyChestC.setIngredient('N', Material.NETHER_STAR);

		Bukkit.addRecipe(flyChestC);
	}

	private void registerGlow() {
		try {
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			NamespacedKey key = new NamespacedKey(this, getDescription().getName());

			Glow glow = new Glow(key);
			Enchantment.registerEnchantment(glow);
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

