package fr.Antaria.antaria.listeners;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {

	@EventHandler
	public void onInventoryClicked(InventoryClickEvent e) {
		if(e.getSlotType() != SlotType.ARMOR) return;
		Player p = (Player) e.getWhoClicked();

		if(p.getGameMode() != GameMode.CREATIVE || p.getGameMode() != GameMode.SPECTATOR) {
			if(e.getSlot() == 38) {
				if(e.getCursor().getType() == Material.CHAINMAIL_CHESTPLATE) {
					p.sendMessage("�aYou can now fly !");
					p.setAllowFlight(true);
				} else {
					p.sendMessage("�cYou can't fly anymore !");
					p.setAllowFlight(false);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if(p.getInventory().getChestplate().getType() == Material.CHAINMAIL_CHESTPLATE) {
			p.setAllowFlight(true);
			p.setFlying(true);
		}
	}
	
}
